const express = require("express");
const cors = require('cors')
const app = express();

app.use(cors())

const data = require("./resources/status.json");


app.get('/', (req, res) => {
    data.sort((a, b) => {
        // Sortataan aakkosjärjestykseen
        let fa = a["Package"].toLowerCase(),
            fb = b["Package"].toLowerCase();
        if (fa < fb) {
            return -1;
        }
        if (fa > fb) {
            return 1;
        }
        return 0;
    });
    res.send(data);
})

app.get('/:name', (req, res) => {
    const name = req.params.name;

    for (let i = 0; i < data.length; i++) {
        if (data[i].Package === name) {
            res.send(data[i])
            console.log(data[i])
            return
        }
    }

    res.send({error: "Ei löytynyt"})
})

app.listen(3001)


