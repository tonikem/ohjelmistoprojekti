import React from 'react';

class Package extends React.Component {
    state = {
        package_list: [],
        data: {},
    };

    async componentDidMount(prevProps) {
        const package_list = await fetch("http://localhost:3001")
        this.setState({package_list: await package_list.json()})

        let pathname

        if (window.location.pathname.includes("%20")) {
            pathname = window.location.pathname.split('%20')[0]
        } else {
            pathname = window.location.pathname
        }

        const response = await fetch("http://localhost:3001" + pathname)
        const data = await response.json()
        this.setState({data: data})
    }
    render() {
        let packages

        if (this.state.data.Depends) {
            if (Array.isArray(this.state.data.Depends)) {
                packages = this.state.data.Depends.map((link) => {
                    return <a key={link} href={link}> {link} </a>
                });
            } else {
                const link = this.state.data.Depends
                packages = <a key={link} href={link}> {link} </a>
            }
        }

        let depends_on = []

        this.state.package_list.forEach(p => {
            if (p["Depends"]) {
                if (Array.isArray(p["Depends"])) {
                    p["Depends"].forEach(d => {
                        const package_name = d.split(' ')[0]
                        if (package_name === this.state.data.Package) {
                            depends_on.push(p['Package'])
                        }
                    })
                } else {
                    const package_name = p["Depends"].split(' ')[0]
                    if (package_name === this.state.data.Package) {
                        depends_on.push(p['Package'])
                    }
                }
            }
        })

        const depends_on_mapped = depends_on.map((link) => {
            return <a key={link} href={link}> {link} </a>
        });

        return <div className="Text">
            <h2>Name: {this.state.data.Package}</h2>
            <p>Status: {this.state.data.Status}</p>
            <p>Priority: {this.state.data.Priority}</p>
            <p>Depends: {packages}</p>
            <p>Packages that depend on this: {depends_on_mapped}</p>
        </div>
    }
}

export default Package;