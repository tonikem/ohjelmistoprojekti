import React from 'react';
import './App.css';

class Home extends React.Component {
    state = {
        data: [],
    };

    async componentDidMount(prevProps) {
        const response = await fetch("http://localhost:3001/")
        const data = await response.json()
        this.setState({data: data})
    }
    render() {
        const packages = this.state.data.map((link) =>
            <div key={"http://localhost:3001/" + link.Package}>
                <a href={link.Package}><h2>{link.Package}</h2></a>
                <p>{link.Description}</p>
            </div>
        );

        return <div className="Text">
            {packages}
        </div>
    }
}

export default Home;
